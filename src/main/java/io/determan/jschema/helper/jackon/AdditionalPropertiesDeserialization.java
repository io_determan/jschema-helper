package io.determan.jschema.helper.jackon;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdditionalPropertiesDeserialization extends DeserializationProblemHandler {

    boolean objectStart = false;
    boolean arrayStart = false;

    @Override
    public boolean handleUnknownProperty(DeserializationContext ctxt, JsonParser p, JsonDeserializer<?> deserializer, Object beanOrClass, String propertyName) throws IOException {

        try {
            Class BeanClass = beanOrClass.getClass();
            Field field = BeanClass.getDeclaredField("additionalProperties");
            field.setAccessible(true);
            Map<String, Object> properties = getMap(field, beanOrClass);

            Object value = null;
            switch (p.getCurrentToken()) {
                case VALUE_STRING:
                    value = p.getValueAsString();
                    break;
                case VALUE_NUMBER_INT:
                    value = p.getValueAsInt();
                    break;
                case VALUE_NUMBER_FLOAT:
                    value = p.getFloatValue();
                    break;
                case VALUE_FALSE:
                    value = Boolean.FALSE;
                    break;
                case VALUE_TRUE:
                    value = Boolean.TRUE;
                    break;
                case VALUE_EMBEDDED_OBJECT:
                    value = p.getEmbeddedObject();
                    break;
                case START_OBJECT:
                    objectStart = true;
                    value = p.readValueAs(HashMap.class);
                    break;
                case END_OBJECT:
                    objectStart = false;
                    break;
                case START_ARRAY:
                    arrayStart = true;
                    value = p.readValueAs(ArrayList.class);
                    break;
                case END_ARRAY:
                    arrayStart = false;
                    break;
                case VALUE_NULL:
                default:
                    value = null;
            }
            if (value != null) {
                properties.put(propertyName, value);
            }
        } catch (Exception e) {
            String unknownField = String.format("Ignoring unknown property %s while deserializing %s", propertyName, beanOrClass);
            System.out.println(getClass().getSimpleName() + " " + unknownField);
        }
        return true;
    }

    private Map<String, Object> getMap(Field field, Object object) {
        Object properties = null;
        try {
            properties = field.get(object);
            if (properties == null) {
                properties = new HashMap<>();
                field.set(object, properties);
            }
        } catch (Exception e) {

        }
        return (Map<String, Object>) properties;
    }
}