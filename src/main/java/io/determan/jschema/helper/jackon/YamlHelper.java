package io.determan.jschema.helper.jackon;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public class YamlHelper {

    public static ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.addHandler(new AdditionalPropertiesDeserialization());
        mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
        return mapper;
    }

}
