package io.determan.jschema.jackson;

import com.google.common.io.Files;
import io.determan.beans.Address;
import io.determan.beans.Person;
import io.determan.jschema.helper.jackon.JsonHelper;
import io.determan.jschema.helper.jackon.YamlHelper;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.Map;

public class BasicTest {



    protected String getSources(String path) throws Exception {
        return Files.asCharSource(new File(Paths.get(path).toUri()), Charset.forName("UTF-8")).read();
    }

    @Test
    public void jsonTest() throws Exception {
        String json = getSources("src/test/resources/basic.json");
        Person person = JsonHelper.objectMapper().readValue(json, Person.class);
        assertPerson(person);
    }


    @Test
    public void ymlTest() throws Exception {
        String yml = getSources("src/test/resources/basic.yml");
        Person person = YamlHelper.objectMapper().readValue(yml, Person.class);
        assertPerson(person);
    }

    private void assertPerson(Person person) {
        Assertions.assertThat(person.getName()).isEqualTo("John");
        Assertions.assertThat(person.getActive()).isNull();
        Assertions.assertThat(person.getAdditionalProperties()).isNull();
        Assertions.assertThat(person.getAddresses()).hasSize(1);

        Address address = person.getAddresses()[0];
        Assertions.assertThat(address.getStreet1()).isEqualTo("123 Acme St.");
        Assertions.assertThat(address.getStreet2()).isNull();
        Assertions.assertThat(address.getAdditionalProperties()).hasSize(4);

        Map<String, Object> properties = address.getAdditionalProperties();

        Assertions.assertThat(properties.get("name")).isEqualTo("Home Address");
        Assertions.assertThat(properties.get("city")).isEqualTo("New York");
        Assertions.assertThat(properties.get("state")).isEqualTo("NY");
        Assertions.assertThat(properties.get("zip")).isInstanceOf(Map.class);

        Map<String, Object> zip = (Map<String, Object>) properties.get("zip");

        Assertions.assertThat(zip).hasSize(2);
        Assertions.assertThat(zip.get("code")).isEqualTo(12345);
        Assertions.assertThat(zip.get("plus_four")).isEqualTo(1234);
    }


}
