package io.determan.beans;

import java.util.Arrays;
import java.util.Map;

public class Person {

    private String name;
    private Boolean active;
    private Address[] addresses;

    private Map<String, Object> additionalProperties;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Address[] getAddresses() {
        return addresses;
    }

    public void setAddresses(Address[] addresses) {
        this.addresses = addresses;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", active=" + active +
                ", addresses=" + Arrays.toString(addresses) +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
